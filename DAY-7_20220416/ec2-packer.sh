#!/bin/bash 

# Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-083654bd07b5da81d" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-ae87c8f1" \
--security-group-ids "sg-03cb43ecdf16cd940" \
--key-name "aws-kesav" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Packer},{Key=Type,Value=To-Bake-Golden-Images},{Key=CreatedBy,Value=Using-AWSCLI-UsingShellScript}]' \
--user-data file://install-packer-using-apt-ubuntu.txt --profile devops 