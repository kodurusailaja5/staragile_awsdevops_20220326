#!/bin/bash 

# Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-04505e74c0741db8d" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-ae87c8f1" \
--security-group-ids "sg-03cb43ecdf16cd940" \
--key-name "aws-kesav" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=CB-CM-AnsibleController},{Key=Type,Value=CBCRs},{Key=CreatedBy,Value=Using-AWSCLI-UsingShellScript}]' \
--user-data file://install-ansible-controller.txt --profile devops 