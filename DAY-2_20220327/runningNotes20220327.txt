Agenda :

AWS :
    - Downloading, Intalling & Configuring below list of softwares on EC2 instance:
        - WebServers :
            - Nginx 
            - Apache2 : Ubuntu
            - Httpd : Redhat, CentOS, Amazonlinux etc
            - IIS : Windows 

        - Database :
            - MySQL
            - Oracle
            - MSSQL
            - PSQL

        - Application Servers :
            Apache Tomcat / Glassfish / Jboss / Weblogic / WebSphere / IIS 
        - Continuous Integration 
            - Jenkins 
        - Continuous Binary Code Repository :
            - Jfrog / Nexus / AWS Artifact / Azure Artifact
        - Continuous Code Quality :
            - Sonarqube

        Java Code :
            - gitlab : https://gitlab.com/cloudbinary/codewithck

Q?