#!/bin/bash 

# Windows 
aws ec2 run-instances \
--image-id "ami-07d6a45d93fcb8a8e" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-421fef29" \
--security-group-ids "sg-0d7f618c300003c9e" \
--key-name "staragile_aws" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Win-IIS},{Key=CreatedBy,Value=StarAgile}]' \
--user-data file://install-IIS.txt --profile devops  